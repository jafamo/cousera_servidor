//modulo de rutas de expres
var express = require('express');

// el router de express
var router = express.Router();

//
var bicicletaController = require('../controllers/bicicleta');

//definir la ruta base para el listado de bicis
router.get('/', bicicletaController.bicicleta_list);

//rutas para creación de bicics
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.Bicicleta_create_post);

//router para eliminar bici
router.post('/:id/delete', bicicletaController.Bicicleta_delete_post);


//router UPDATE
router.get('/:id/update', bicicletaController.Bicicleta_update_get);
router.post('/:id/update',bicicletaController.Bicicleta_update_post);


//modulo de bicicletas
module.exports = router;
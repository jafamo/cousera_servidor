//modulo de rutas de expres
var express = require('express');

// el router de express
var router = express.Router();

var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/',bicicletaController.bicicleta_list);

//Add route crete bicicleta:
router.post('/create',bicicletaController.bicicleta_create);

router.post('/delete',bicicletaController.bicicleta_delete);

router.put('/:id/update', bicicletaController.bicicleta_update);
module.exports=router;

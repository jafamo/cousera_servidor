var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

//prototipo para redifinir el toString
Bicicleta.prototype.toString = function (){

    return 'id: ' + this.id + " | color: " + this.color;
}


//Usar esto para evitar base de datos
Bicicleta.allBicis = [];

//Add
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
var b = new Bicicleta(2, 'azul', 'urbana', [-34.596932, -58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);

//REMOVE
Bicicleta.findId = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta en con el id: ${aBiciId}`);
}

Bicicleta.removeById = function (aBiciId){
   // var aBici = Bicicleta.findId(aBiciId);
    for (var i=0; i< Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}



//module exports para que otro puedan importar
module.exports = Bicicleta;
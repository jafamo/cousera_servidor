//importamos el modelo.
var Bicicleta = require('../../models/bicicleta');

//function API GET Bicicleta
exports.bicicleta_list = function(req, res){
    res.status (200).json({
        bicicletas: Bicicleta.allBicis
    });
}

//function API create bicicleta
exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];

    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = (req, res) => {
    let bicicleta = entity.findById(req.params.id);
    bicicleta.id = req.body.id;
    bicicleta.color = req.body.color;
    bicicleta.modelo = req.body.modelo;
    bicicleta.ubicacion = [req.body.lat, req.body.long]
  
    res.status(200).json({
      data: bicicleta
    });
  }
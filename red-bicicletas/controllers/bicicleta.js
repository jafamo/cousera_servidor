//importar el modulo de bicicleta
var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    //renderizar la vista
    res.render('../views/bicicletas/index', {bicis:Bicicleta.allBicis});
}

//acceder a la pagina de creación
exports.bicicleta_create_get = function(req, res){
    res.render('../views/bicicletas/create');
}

//Acción de creación 
exports.Bicicleta_create_post = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];

    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

//Acción de borrado
exports.Bicicleta_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');

}

//Acción de Update
exports.Bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findId(req.params.id);
    res.render('../views/bicicletas/update', {bici});
}


exports.Bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findId(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.long];
    //Bicicleta.add(bici);

    res.redirect('/bicicletas');

    
}





var map = L.map('main_map').setView([-34.6012424, -58.3861497], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
   
}).addTo(map);

//cargar nuevas bicicletas en el map

L.marker([-34.6012424, -58.3861497]).addTo(map);
L.marker([-34.596932, -58.3808287]).addTo(map);

//llamada AJAX 

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        //para cada bicicleta, mostrar en el marcador el ID
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map);
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map);
            
        });
    }
})